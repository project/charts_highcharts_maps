<?php

/**
 * @file
 * Installation and update hooks for the Charts Highmap module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function charts_highcharts_maps_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the charts_highcharts_maps module.
    case 'help.page.charts_highcharts_maps':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('This module extends the Charts module to enable maps using the Highcharts Maps library.') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_requirements().
 */
function charts_highcharts_maps_requirements($phase) {
  $requirements = [];

  switch ($phase) {
    case 'runtime':
      $library_path = charts_highcharts_maps_find_library();
      $config = \Drupal::service('config.factory')->getEditable('charts.settings');
      $cdn = $config->get('advanced.requirements.cdn') ?? FALSE;

      if (!$library_path) {
        if ($cdn) {
          $requirements['charts_highcharts_maps_js'] = [
            'title' => t('Highmap Library'),
            'value' => t('Available through a CDN'),
            'severity' => REQUIREMENT_WARNING,
            'description' => t('You are using the Highcharts Maps library via a content delivery network. It is generally considered better practice to install the library files locally. Please see the README file inside charts_highcharts_maps for instructions to install the library.'),
          ];
        }
        else {
          $requirements['charts_highcharts_maps_js'] = [
            'title' => t('Highmap Library'),
            'value' => t('Not Installed'),
            'severity' => REQUIREMENT_ERROR,
            'description' => t('You are missing the Highcharts Maps library in your Drupal installation directory and you have opted not to use a CDN. Please either enable use of the CDN in the Chart Settings under the Advanced tab or see the README file inside charts_highcharts_maps for instructions to install the library.'),
          ];
        }
      }
      else {
        $requirements['charts_highcharts_maps_js'] = [
          'title' => t('Highmap Library'),
          'severity' => REQUIREMENT_OK,
          'value' => t('Installed'),
        ];
      }
      break;
  }

  if (function_exists('libraries_detect') && $highmap_info = libraries_detect('highmap')) {
    if (is_dir($highmap_info['library path'] . '/js/exporting-server')) {
      $requirements['highmap_security'] = [
        'title' => t('highmap vulnerability'),
        'severity' => REQUIREMENT_ERROR,
        'value' => t('Dangerous sample code present'),
        'description' => t('Your installation of the Highmap library at "@path" contains a directory named "exporting-server". This directory contains dangerous sample files that may compromise the security of your site. You must delete this directory before you may use the Charts Highmap module.', ['@path' => $highmap_info['library path']]),
      ];
    }
  }

  // The charts_highcharts or the charts_highstock module should be enabled.
  $dependency_requirements_met = FALSE;
  if (\Drupal::moduleHandler()->moduleExists('charts_highcharts') || \Drupal::moduleHandler()->moduleExists('charts_highstock')) {
    $dependency_requirements_met = TRUE;
  }
  $requirements['charts_highcharts_dependency'] = [
    'title' => t('Highcharts Maps Library Dependency'),
    'severity' => $dependency_requirements_met ? REQUIREMENT_OK : REQUIREMENT_ERROR,
    'value' => $dependency_requirements_met ? t('Enabled') : t('Not Enabled'),
    'description' => $dependency_requirements_met ? t('The Charts Highcharts or Charts Highstock module is enabled.') : t('The Charts Highcharts or Charts Highstock module must be enabled for the Charts Highmap module to work.'),
  ];

  return $requirements;
}

/**
 * Get the location of the highmap library.
 *
 * @return string
 *   The location of the library, or FALSE if the library isn't installed.
 */
function charts_highcharts_maps_find_library() {
  // The following logic is taken from libraries_get_libraries()
  $searchdir = [];

  // Similar to 'modules' and 'themes' directories inside an installation
  // profile, installation profiles may want to place libraries into a
  // 'libraries' directory.
  $searchdir[] = 'profiles/' . \Drupal::installProfile() . '/libraries';

  // Always search libraries.
  $searchdir[] = 'libraries';

  // Also search sites/<domain>/*.
  $container = \Drupal::getContainer();
  $site_path = $container->getParameter('site.path');
  $searchdir[] = $site_path . '/libraries';

  foreach ($searchdir as $dir) {
    if (file_exists($dir . '/highcharts_maps/map.js')) {
      return $dir . '/highcharts_maps';
    }
  }

  return FALSE;
}
